import requests
import os
import zipfile
from bs4 import BeautifulSoup
from tqdm import tqdm

url_list_file = "../urls.txt"
index_file = "index.txt"
path_to_download = '../pages/'
zip_file = "pages.zip"

with open(url_list_file, "r") as f:
    urls = f.readlines()

if not os.path.exists(path_to_download):
    os.makedirs(path_to_download)

with open(index_file, "w") as index_f:
    for index, url in enumerate(tqdm(urls, desc="Downloading pages", unit="page")):
        url = url.strip()

        response = requests.get(url)
        if response.status_code == 200:
            soup = BeautifulSoup(response.content, "html.parser")

            html_content = response.content

            js_links = [link.get("src") for link in soup.find_all("script") if link.get("src")]

            filename = f"{path_to_download}{index}.txt"
            with open(filename, "wb") as f:
                f.write(html_content)

                f.write(b"\n\nJavaScript links:\n")
                for link in js_links:
                    f.write(f"{link}\n".encode())

            print(f"Downloaded page {url}")

            index_f.write(f"{filename}\t{url}\n")
            
        if os.path.exists(path_to_download):
            with zipfile.ZipFile(zip_file, "w") as zip_f:
                for foldername, subfolders, filenames in os.walk(path_to_download):
                    for filename in filenames:
                        file_path = os.path.join(foldername, filename)
                        zip_f.write(file_path)
        else:
            print(f"Error: Could not download page {url}")
