from fastapi import FastAPI

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel
import os
import glob

app = FastAPI()

directory_path = "../task_2/tokenizer/"


def read_tokens(path):
    with open(path, "r") as file:
        text = file.read()
    return ' '.join(text.split('\n'))


def load_documents():
    documents = {}
    index_file = 'index.txt'
    with open(index_file, "r") as file:
        for line in file:
            file_path, name_href = line.split()
            file_path_token = os.path.join(directory_path, file_path[9:] + "_tokens.txt")
            documents[name_href] = read_tokens(file_path_token)
    return list(documents.values()), list(documents.keys())


documents_list, documents_names = load_documents()


@app.post("/search")
async def search(search_query: str = 'транскрипция речи'):
    vectorizer = TfidfVectorizer()
    doc_vectors = vectorizer.fit_transform([search_query] + documents_list)

    cosine_similarities = linear_kernel(doc_vectors[0:1], doc_vectors).flatten()
    document_scores = [item.item() for item in cosine_similarities[1:]]

    document_scores_dict = {}
    for i, filename in enumerate(documents_names):
        document_scores_dict[filename] = document_scores[i]

    sorted_keys = sorted(document_scores_dict, key=document_scores_dict.get, reverse=True)

    results = []
    for key in sorted_keys:
        if document_scores_dict[key] > 0:
            results.append({"href": key, "score": document_scores_dict[key]})

    return {"search_query": search_query, "results": results}
