import os
from collections import defaultdict

path_to_tokenizer = '../task_2/tokenizer/'


def build_inverted_index(path_to_tokenizer):
    inverted_index = defaultdict(set)

    for filename in os.listdir(path_to_tokenizer):
        if filename.endswith("_lemmas.txt"):
            with open(f"{path_to_tokenizer}/{filename}", "r", encoding="utf-8") as f:
                for line in f:
                    lemma, _ = line.strip().split(":")
                    inverted_index[lemma].add((filename[:-15]))

    return inverted_index


if __name__ == '__main__':
    inverted_index = build_inverted_index(path_to_tokenizer)
    with open('inverted_index.txt', 'w', encoding='utf-8') as f:
        for token, filenames in inverted_index.items():
            f.write(f"{token} {','.join(sorted(filenames))}\n")
