import json
import os
import spacy

nlp = spacy.load("ru_core_news_sm")

def load_inverted_index(filename):
    inverted_index = {}
    with open(filename, "r", encoding="utf-8") as f:
        for line in f:
            term, doc_ids_str = line.strip().split(' ', 1)
            doc_ids = [int(x) for x in doc_ids_str.split(',')]
            inverted_index[term] = doc_ids
    return inverted_index


def boolean_search(query, index):
    query_terms = [token.lemma_.lower() for token in nlp(query) if not token.is_punct]

    for term in query_terms:
        if term not in index and term not in ['и', 'или', 'не']:
            return f"{term} нет в поиске"

    doc_ids = set(index[query_terms[0]])

    # Apply the boolean operators to the document ids
    for i in range(1, len(query_terms), 2):
        if query_terms[i] == "и":
            doc_ids &= set(index[query_terms[i + 1]])
        elif query_terms[i] == "или":
            doc_ids |= set(index[query_terms[i + 1]])
        elif query_terms[i] == "не":
            doc_ids -= set(index[query_terms[i + 1]])

    return doc_ids


if __name__ == '__main__':
    index_path = "inverted_index.txt"
    index = load_inverted_index(index_path)
    print(index)
    query = "книга и нейронная или статья и рыбы"
    doc_ids = boolean_search(query, index)
    print(f"Query: {query}")
    print(f"Count Articles: {len(doc_ids)} \nResult: {doc_ids}")

    with open("search_results.txt", "w", encoding="utf-8") as f:
        f.write(f"Query: {query}\n")
        f.write(f"Count Articles: {len(doc_ids)}\n")
        f.write("Result:\n")
        for doc_id in doc_ids:
            f.write(f"{doc_id} ")

