from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel
from nltk import word_tokenize
from nltk.stem import WordNetLemmatizer
import nltk
from nltk.corpus import stopwords
import os
import glob
from sklearn.feature_extraction.text import TfidfVectorizer

path_of_files = "../task_2/tokenizer"


def read_tokens(path):
    with open(path, "r") as file:
        text = file.read()
    return ' '.join(text.split('\n'))


directory_path = "../task_2/tokenizer/"

documents = dict()
index_file = 'index.txt'
with open(index_file, "r") as file:
    for line in file:
        file_path, name_href = line.split()
        file_path_token = (os.path.join(directory_path, file_path[9:] + "_tokens.txt"))
        documents[name_href] = read_tokens(file_path_token)
documents_list = list(documents.values())

search_terms = 'Транскрипция речи'
vectorizer = TfidfVectorizer()
doc_vectors = vectorizer.fit_transform([search_terms] + documents_list)
#
cosine_similarities = linear_kernel(doc_vectors[0:1], doc_vectors).flatten()
document_scores = [item.item() for item in cosine_similarities[1:]]


document_scores_dict = {}
for i, filename in enumerate(documents.keys()):
    document_scores_dict[filename] = document_scores[i]

sorted_keys = sorted(document_scores_dict, key=document_scores_dict.get, reverse=True)
answer = dict()
for key in sorted_keys:
    if document_scores_dict[key] > 0:
        answer[key] = document_scores_dict[key]
