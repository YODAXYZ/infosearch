import os
import glob
from sklearn.feature_extraction.text import TfidfVectorizer

path_of_files = "../task_2/tokenizer"


def read_tokens(path):
    # Read the contents of the file
    all_text = list()
    with open(path, "r") as file:
        text = file.read()
    return ' '.join(text.split('\n'))


directory_path = "../task_2/tokenizer/"
output_path = "tokenizer"
if not os.path.exists(output_path):
    os.makedirs(output_path)

# Loop through all files in the directory ending in "_token"
documents = set()
for i, file_path in enumerate(glob.glob(os.path.join(directory_path, "*_tokens.txt"))):
    documents.add(read_tokens(file_path))

vectorizer = TfidfVectorizer()
tf_idf_values = vectorizer.fit_transform(documents)
feature_names = vectorizer.get_feature_names_out()
idf_values = vectorizer.idf_
tf_idf_dict = {}

for i in range(len(documents)):
    output_file_path = os.path.join(output_path, f'{i}_token_tfidf.txt')
    print(output_file_path)
    for feature_index in tf_idf_values.indices:
        feature_name = feature_names[feature_index]
        tf_idf_dict[feature_name] = [tf_idf_values[i, feature_index] * idf_values[feature_index], tf_idf_values[i, feature_index], idf_values[feature_index]]

    with open(output_file_path, "w") as output_file:
        for feature_name, tf_idf_value in tf_idf_dict.items():
            output_file.write(f"{feature_name} {tf_idf_dict[feature_name][2]} {tf_idf_dict[feature_name][0]}\n")

    tf_idf_dict = {}