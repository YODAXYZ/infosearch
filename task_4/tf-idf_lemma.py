import os
import glob
import spacy
from sklearn.feature_extraction.text import TfidfVectorizer
from tqdm import tqdm

def read_text(path):
    # Read the contents of the file
    with open(path, "r") as file:
        text = file.read()
    return text

def is_garbage(word):
    """
    Check if a word is garbage, i.e. a preposition, conjunction,
    number or containing digits or special characters.
    And also check ru lang
    """
    if not word.is_alpha or word.is_ascii or not word.is_lower or word.is_stop or word.lang_ != "ru":
        return True

    if word.pos_ in ["ADP", "CCONJ", "NUM"]:
        return True

    return False


def get_lemmas(text):
    """
    Get a list of lemmas
    """
    nlp = spacy.load("ru_core_news_sm")
    lemmas = []

    doc = nlp(text)
    for token in doc:
        if not is_garbage(token):
            lemmas.append(token.lemma_)
    return lemmas

directory_path = "../task_2/tokenizer/"
output_path = "tokenizer_lemmas"

if not os.path.exists(output_path):
    os.makedirs(output_path)

# Loop through all files in the directory ending in "_tokens.txt"
documents = []
for i, file_path in tqdm(enumerate(glob.glob(os.path.join(directory_path, "*_tokens.txt")))):
    print(i)
    text = read_text(file_path)
    lemmas = get_lemmas(text)
    documents.append(' '.join(lemmas))

# Compute TF-IDF values
vectorizer = TfidfVectorizer()
tf_idf_values = vectorizer.fit_transform(documents)
feature_names = vectorizer.get_feature_names_out()
idf_values = vectorizer.idf_
tf_idf_dict = {}

for i in range(len(documents)):
    output_file_path = os.path.join(output_path, f'{i}_lemma_tfidf.txt')
    print(output_file_path)
    for feature_index in tf_idf_values.indices:
        feature_name = feature_names[feature_index]
        tf_idf_dict[feature_name] = [tf_idf_values[i, feature_index] * idf_values[feature_index], tf_idf_values[i, feature_index], idf_values[feature_index]]

    with open(output_file_path, "w") as output_file:
        for feature_name, tf_idf_value in tf_idf_dict.items():
            output_file.write(f"{feature_name} {tf_idf_dict[feature_name][2]} {tf_idf_dict[feature_name][0]}\n")

    tf_idf_dict = {}