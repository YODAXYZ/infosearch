import requests
from bs4 import BeautifulSoup

# URL of the website to scrape
base_url = "https://habr.com/ru/hub/artificial_intelligence/"

# Text file to store the URLs
output_file = "urls.txt"

urls = set()

page_num = 11
for i in range(1, page_num):
    page_url = f"{base_url}page{i}"
    response = requests.get(page_url)
    soup = BeautifulSoup(response.content, "html.parser")
    
    for article in soup.find_all('article'):
        link = article.select_one('h2 > a').get('href')
        full_link = f"https://habr.com{link}"
        
        if len(urls) < 100:
            urls.add(full_link)
        else: 
            break

print(len(urls))
# Write all unique URLs to the text file
with open(output_file, "w") as file:
    for url in urls:
        file.write(f"{url}\n")