import re
import os
import pymorphy2
import spacy 
from tqdm import tqdm
from collections import defaultdict

path_to_download = '../pages/'
path_to_tokenizer = 'tokenizer/'
nlp = spacy.load("ru_core_news_sm")

def is_garbage(word):
    """
    Check if a word is garbage, i.e. a preposition, conjunction,
    number or containing digits or special characters.
    And also check ru lang 
    """
    if not word.is_alpha or word.is_ascii or not word.is_lower or word.is_stop or word.lang_ != "ru":
        return True

    if word.pos_ in ["ADP", "CCONJ", "NUM"]:
        return True

    return False


def get_tokens_and_lemma(filename):
    """
    Get a list of tokens and lemmas
    """

    tokens = []
    lemmas = []

    with open(filename, "r", encoding="utf-8") as f:
        text = f.read()
        doc = nlp(text)

        for token in doc:
            if not is_garbage(token):
                tokens.append(token.text)
                lemmas.append(token.lemma_)
                
    return tokens, lemmas


if __name__ == '__main__':
    for filename in tqdm(os.listdir(path_to_download)):
        tokens, lemmas = get_tokens_and_lemma(f"{path_to_download}{filename}")
        
        # Group tokens by their lemmas
        lemma_to_tokens = defaultdict(list)
        for token, lemma in zip(tokens, lemmas):
            lemma_to_tokens[lemma].append(token)
            
        if not os.path.exists(path_to_tokenizer):
            os.makedirs(path_to_tokenizer)
            
        with open(f"{path_to_tokenizer}{filename}_tokens.txt", "w", encoding="utf-8") as f:
            f.writelines([token + '\n' for token in tokens])

        with open(f"{path_to_tokenizer}{filename}_lemmas.txt", "w", encoding="utf-8") as f:
            for lemma, tokens in lemma_to_tokens.items():
                f.write(f"{lemma}: {' '.join(set(tokens))}\n")
        
        
        

        